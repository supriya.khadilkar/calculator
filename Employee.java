package oopproblems;

import java.io.Serializable;

public class Employee implements Serializable
{
    private int empId;
    private String empName;
    private int salary;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    Employee(int id,String name, int salary)
    {
        this.empId = id;
        this.empName = name;
        this.salary = salary;
    }
}
