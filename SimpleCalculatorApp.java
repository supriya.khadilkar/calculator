package oopproblems;

import java.util.Random;

public class SimpleCalculatorApp
{
    public static void main(String[] args) {
        SimpleCalculator sc = new SimpleCalculator();

        Random random = new Random();

        /*integer division*/
        System.out.println(sc.divide(66,5));

        /*long multiplication*/
        System.out.println(sc.multiply(5l, 7l));

        /*double addition*/

        System.out.println(sc.add(random.nextDouble(), random.nextDouble()));

        /*float-point subtraction*/
        System.out.println(sc.subtract(random.nextFloat(), random.nextFloat()));




    }
}
