package oopproblems;

public interface Division extends Operation 
{
     double divide(int number1, int number2) throws ArithmeticException;
     double divide(float number1, float number2) throws ArithmeticException;
     double divide(double number1, double number2) throws ArithmeticException;
     double divide(long number1, long number2) throws ArithmeticException;
}
