package oopproblems;

public interface Subtraction extends Operation
{
     int subtract(int number1, int number2);
     float subtract(float number1, float number2);
     double subtract(double number1, double number2);
     long subtract(long number1, long number2);
}
