package oopproblems;

public class SimpleCalculator implements Multiplication, Addition, Subtraction, Division
{
    @Override
    public int add(int number1, int number2)
    {
        return number1 + number2;
    }

    @Override
    public float add(float number1, float number2)
    {
        return number1 + number2;
    }

    @Override
    public double add(double number1, double number2) {
        return number1 + number2;
    }

    @Override
    public long add(long number1, long number2) {
        return number1 + number2;
    }

    @Override
    public double divide(int number1, int number2) throws ArithmeticException
    {
        if(number2 == 0)
            throw new ArithmeticException("Dividing number by zero.");


        return number1/number2;
    }

    @Override
    public double divide(float number1, float number2) throws ArithmeticException
    {
        if(number2 == 0)
            throw new ArithmeticException("Dividing number by zero.");

        return number1/number2;
    }

    @Override
    public double divide(double number1, double number2) throws ArithmeticException
    {
        if(number2 == 0)
            throw new ArithmeticException("Dividing number by zero.");

        return number1/number2;
    }

    @Override
    public double divide(long number1, long number2) throws ArithmeticException
    {
        if(number2 == 0)
            throw new ArithmeticException("Dividing number by zero.");

        return number1/number2;
    }

    @Override
    public int multiply(int number1, int number2) {
        return number1 * number2;
    }

    @Override
    public float multiply(float number1, float number2) {
        return number1 * number2;
    }

    @Override
    public double multiply(double number1, double number2) {
        return number1 * number2;
    }

    @Override
    public long multiply(long number1, long number2) {
        return number1 * number2;
    }

    @Override
    public int subtract(int number1, int number2) {
        return number1 - number2;
    }

    @Override
    public float subtract(float number1, float number2) {
        return number1 - number2;
    }

    @Override
    public double subtract(double number1, double number2) {
        return number1 - number2;
    }

    @Override
    public long subtract(long number1, long number2) {
        return number1 - number2;
    }
}
