package oopproblems;

public class EmployeeSalaryChecker
{
    public static void main(String[] args) {
        Employee e1 = new Employee(1,"Supriya", 500);
        Employee e2 = new Employee(2, "ABC", 500);


        Employee e3 = new Employee(3,"TXT", 1899);
        Employee e4 = new Employee(4, "UUU", 500);


        if(salaryEqualityCheck(e1,e2))
            System.out.println("Salary is equal");
        else
            System.out.println("Salary is not equal");


        if(salaryEqualityCheck(e3,e4))
            System.out.println("Salary is equal");
        else
            System.out.println("Salary is not equal");

    }

    public static boolean salaryEqualityCheck(Employee e1, Employee e2)
    {
        if(e1.getSalary() == e2.getSalary())
            return true;
        else
            return false;
    }
}
