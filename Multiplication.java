package oopproblems;

public interface Multiplication extends Operation
{
     int multiply(int number1, int number2);
     float multiply(float number1, float number2);
     double multiply(double number1, double number2);
     long multiply(long number1, long number2);
}
